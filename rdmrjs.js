
/*!
 * Readmore JS
 * 
 * Author: Barış Babacanoğlu
 *
 */
(function($) {
	"use strict";

	$.fn.rdmrjs = function(param) {
		var element = this;

		if (($(this).length <1)){
			return false;
		}

		var defaults = {
			limit: 600,
			moreText: 'Read More',
			lessText: 'Read Less',
		}

		var options = $.extend({}, defaults, param);

		var linkMore = '<a href="javascript:void(0)" class="rdmr-js-link-more">'+options.moreText+'</a>';
		var linkLess = '<a href="javascript:void(0)" class="rdmr-js-link-less">'+options.lessText+'</a>';

		var string = $(this).html();
		var length = string.length;

		if(length>options.limit){
			var subString = string.substr(0,options.limit);
			$(element).html(subString+'... '+linkMore);
		}

		$(element).on('click','.rdmr-js-link-more',function(){
			$(element).html(string+' '+linkLess);
		})

		$(element).on('click','.rdmr-js-link-less',function(){
			$(element).html(subString+'... '+linkMore);
		})

	}

})(jQuery);